.ONESHELL:

SHELL=/bin/bash
ENV_NAME?=techtarget
CONDA_BASE_ACTIVATE=source $(shell conda info --base)/etc/profile.d/conda.sh; conda activate
ENV_DIR=$(shell conda info --base)/envs/$(ENV_NAME)

create-env:
ifeq (,$(wildcard $(ENV_DIR)))
	@$(CONDA_BASE_ACTIVATE)
	@conda create -y -q -c conda-forge -n $(ENV_NAME) python=3.9.1 gettext=0.21.0
endif

remove-env:
ifneq (,$(wildcard $(ENV_DIR)))
	@$(CONDA_BASE_ACTIVATE)
	@conda env remove -n $(ENV_NAME)
endif

install-requirements:
ifneq (,$(wildcard $(ENV_DIR)))
	@$(CONDA_BASE_ACTIVATE)
	@conda activate $(ENV_NAME)
	@pip install -r requirements.txt
endif

install-tools:
ifneq (,$(wildcard $(ENV_DIR)))
	@$(CONDA_BASE_ACTIVATE)
	@conda activate $(ENV_NAME)
	@pip install pre-commit==2.17.0 flake8==4.0.1 bandit==1.7.1 gitlint==0.17.0 black==22.6.0
endif

install-pre-commit-hooks:
ifneq (,$(wildcard $(ENV_DIR)))
	@$(CONDA_BASE_ACTIVATE)
	@conda activate $(ENV_NAME)
	@pre-commit install -f -t pre-commit --hook-type commit-msg
endif

uninstall-pre-commit-hooks:
ifneq (,$(wildcard $(ENV_DIR)))
	@$(CONDA_BASE_ACTIVATE)
	@conda activate $(ENV_NAME)
	@pre-commit uninstall -t pre-commit -t commit-msg
endif

install:
	@$(MAKE) create-env
	@$(MAKE) install-tools
	@$(MAKE) install-pre-commit-hooks
	@$(MAKE) install-requirements

uninstall:
	@$(MAKE) uninstall-pre-commit-hooks
	@$(MAKE) remove-env

reinstall:
	@$(MAKE) uninstall
	@$(MAKE) install

.PHONY: create-env install-requirements install-tools install-pre-commit-hooks install uninstall reinstall uninstall-pre-commit-hooks
