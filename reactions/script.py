import psycopg2 as db_connect


def myConn():
    host_name = "localhost"
    db_user = "postgres"
    db_password = "postgres"
    db_name = "techtarget"
    return db_connect.connect(
        host=host_name, user=db_user, password=db_password, database=db_name
    )


connect = myConn()


def export_reactions(connect):

    reaction_name = ["Love", "Like", "Dislike"]
    slug_name = ["Love", "Like", "Dislike"]

    sql = """INSERT INTO public.social_reaction(reaction_name, slug_name) VALUES (%s, %s)""" # noqa
    try:
        cur = connect.cursor()
        for i in range(len(reaction_name)):
            cur.execute(sql, (reaction_name[i], slug_name[i]))
            connect.commit()

    except: # noqa
        print("erro ao exportar reactions")


export_reactions(connect)
