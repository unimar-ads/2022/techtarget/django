from django.contrib import admin
from base.models.gender import Gender
from base.models.segment import Segment
from base.models.civil_status import Civil_stat
from base.models.country import Country_city, Country_region, Country_state
from base.models.user import (
    Common_user,
    User_cnpj,
    User_cpf,
    User_cpf_interest,
)
from base.models.forms import Form, Question, Question_type, User_answer


class GenderAdmin(admin.ModelAdmin):

    list_display = ["name", "description"]
    list_filter = ["name"]
    search_fields = ["name"]


class SegementAdmin(admin.ModelAdmin):

    list_display = ["name", "description"]
    list_filter = ["name"]
    search_fields = ["name"]


class CivilStatusAdmin(admin.ModelAdmin):
    list_display = ["status_name", "status_description"]
    list_filter = ["status_name"]
    search_fields = ["status_name"]


class CityAdmin(admin.ModelAdmin):
    list_display = ["city_name"]
    list_filter = ["city_name"]
    search_fields = ["city_name", "state"]


class StateAdmin(admin.ModelAdmin):
    list_display = ["state_name", "acronym"]
    list_filter = ["state_name", "acronym"]
    search_fields = ["state_name", "acronym"]


class RegionAdmin(admin.ModelAdmin):
    list_display = ["region_name", "region_id"]
    list_filter = ["region_name", "region_id"]
    search_fields = ["region_name", "region_id"]


class CommonUserAdmin(admin.ModelAdmin):
    list_display = ["user_name", "user_email"]
    list_filter = ["user_name", "user_email"]
    search_fields = ["user_name", "user_email"]


class UserCpfAdmin(admin.ModelAdmin):
    list_display = ["name", "last_name"]
    list_filter = ["name"]
    search_fields = ["name", "last_name"]


class UserCnpjAdmin(admin.ModelAdmin):
    list_display = ["corporate_name", "cnpj"]
    list_filter = ["corporate_name", "cnpj"]
    search_fields = ["corporate_name", "cnpj"]


class UserCpfInterestAdmin(admin.ModelAdmin):
    list_display = ["user_cpf", "segment"]
    list_filter = ["user_cpf", "segment"]
    search_fields = ["user_cpf", "segment"]


class FormAdmin(admin.ModelAdmin):
    list_display = ["form_title", "form_description"]
    list_filter = ["form_title", "form_description"]
    search_fields = ["form_title", "form_description"]


class QuestionTypeAdmin(admin.ModelAdmin):
    list_display = ["type_title", "type_description"]
    list_filter = ["type_title", "type_description"]
    search_fields = ["type_title", "type_description"]


class QuestionAdmin(admin.ModelAdmin):
    list_display = ["question_title", "question_type"]
    list_filter = ["question_title", "question_type"]
    search_fields = ["question_title", "question_type"]


class UserAnswerAdmin(admin.ModelAdmin):
    list_display = ["user_cpf", "question"]
    list_filter = ["user_cpf", "question"]
    search_fields = ["user_cpf", "question"]


admin.site.register(Gender, GenderAdmin)
admin.site.register(Segment, SegementAdmin)
admin.site.register(Civil_stat, CivilStatusAdmin)
admin.site.register(Country_region, RegionAdmin)
admin.site.register(Country_state, StateAdmin)
admin.site.register(Country_city, CityAdmin)
admin.site.register(Common_user, CommonUserAdmin)
admin.site.register(User_cnpj, UserCnpjAdmin)
admin.site.register(User_cpf, UserCpfAdmin)
admin.site.register(User_cpf_interest, UserCpfInterestAdmin)
admin.site.register(Form, FormAdmin)
admin.site.register(Question_type, QuestionTypeAdmin)
admin.site.register(Question, QuestionAdmin)
admin.site.register(User_answer, UserAnswerAdmin)
