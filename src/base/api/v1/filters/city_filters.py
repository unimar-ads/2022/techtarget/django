from django_filters import FilterSet
from base.models.country import Country_city
from base.api.v1.filters.fields import (
    CharInFilter
)


class CityFilter(FilterSet):
    state = CharInFilter(field_name="state", lookup_expr="in")

    class Meta:
        model = Country_city
        fields = ["state"]
