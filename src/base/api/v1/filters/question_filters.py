from django_filters import FilterSet
from base.models.forms import Question
from base.api.v1.filters.fields import (
    CharInFilter
)


class QuestionFilter(FilterSet):
    form = CharInFilter(field_name="user_form", lookup_expr="in")

    class Meta:
        model = Question
        fields = ["form"]
