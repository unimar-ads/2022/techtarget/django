from django_filters import FilterSet
from base.models.user import User_cnpj, User_cpf
from base.api.v1.filters.fields import (
    CharInFilter
)


class UserCpfFilter(FilterSet):
    user = CharInFilter(field_name="id", lookup_expr="in")

    class Meta:
        model = User_cpf
        fields = ["user"]


class UserCnpjFilter(FilterSet):
    user = CharInFilter(field_name="id", lookup_expr="in")

    class Meta:
        model = User_cnpj
        fields = ["user"]
