from rest_framework import serializers
from base.models.user import (
    Civil_stat,
)


class CivilStatusReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Civil_stat
        fields = "__all__"
