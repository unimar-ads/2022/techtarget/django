from rest_framework import serializers
from base.models.country import (
    Country_city,
    Country_region,
    Country_state,
)


class RegionReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Country_region
        fields = "__all__"


class StateReadSerializer(serializers.ModelSerializer):
    region = RegionReadSerializer()

    class Meta:
        model = Country_state
        fields = ("state_code", "state_name", "acronym", "region")


class CityReadSerializer(serializers.ModelSerializer):
    state = StateReadSerializer()

    class Meta:
        model = Country_city
        fields = '__all__'
