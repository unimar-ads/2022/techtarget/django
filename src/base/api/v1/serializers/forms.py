from rest_framework import serializers
from base.models.forms import Question_type, Form, Question, User_answer
from base.api.v1.serializers.user import UserCpfReadSerializer


class FormsReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Form
        fields = "__all__"


class QuestionsReadSerializer(serializers.ModelSerializer):
    form = FormsReadSerializer(source="user_form")

    class Meta:
        model = Question
        fields = [
            "id",
            "form",
            "question_title",
            "question_order",
            "question_type",
        ]


class QuestionTypesReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Question_type
        fields = "__all__"


class UserAnswersReadSerializer(serializers.ModelSerializer):
    question = QuestionsReadSerializer()
    user_cpf = UserCpfReadSerializer()

    class Meta:
        model = User_answer
        fields = ("user_cpf", "question", "answer_field")


class UserAnswersInsertSerializer(serializers.ModelSerializer):
    class Meta:
        model = User_answer
        fields = "__all__"


class QuestionsInsertSerializer(serializers.ModelSerializer):
    class Meta:
        model = Question
        fields = "__all__"
