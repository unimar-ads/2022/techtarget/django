from rest_framework import serializers
from base.models.user import (
    Gender,
)


class GenderReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Gender
        fields = "__all__"
