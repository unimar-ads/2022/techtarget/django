from rest_framework import serializers
from base.models.user import (
    Segment,
)


class SegmentReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Segment
        fields = "__all__"
