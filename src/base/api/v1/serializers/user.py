from rest_framework import serializers
from base.models.user import (
    User_cpf,
    User_cnpj,
    Common_user,
    User_cpf_interest,
)
from base.api.v1.serializers.civil_status import CivilStatusReadSerializer
from base.api.v1.serializers.gender import GenderReadSerializer
from base.api.v1.serializers.country import CityReadSerializer


class UserCpfCommonReadSerializer(serializers.ModelSerializer):

    civil_status = CivilStatusReadSerializer()
    gender = GenderReadSerializer()

    class Meta:
        model = User_cpf
        fields = (
            "id",
            "cpf",
            "name",
            "last_name",
            "civil_status",
            "gender",
        )
        depth = 1


class UserCnpjCommonReadSerializer(serializers.ModelSerializer):

    class Meta:
        model = User_cnpj
        fields = ("id", "cnpj", "corporate_name", "segments")
        depth = 1


class CommonUserReadSerializer(serializers.ModelSerializer):
    city = CityReadSerializer(source="user_city")
    user_photo = serializers.ImageField(
        max_length=None, use_url=True
    )
    user_cnpj_set = UserCnpjCommonReadSerializer(many=True)
    user_cpf_set = UserCpfCommonReadSerializer(many=True)

    class Meta:
        model = Common_user
        fields = (
            "id",
            "user_name",
            "creation_date",
            "user_email",
            "user_phone",
            "user_photo",
            "city",
            "user_active",
            "user_cpf_set",
            "user_cnpj_set"
        )
        read_only_fields = ["user_photo"]


class CommonUserLoginSerializer(serializers.ModelSerializer):
    city = CityReadSerializer(source="user_city")

    class Meta:
        model = Common_user
        fields = (
            "user_name",
            "creation_date",
            "user_email",
            "user_phone",
            "city",
            "user_active",
        )


class UserCnpjReadSerializer(serializers.ModelSerializer):
    common_user = CommonUserReadSerializer()

    class Meta:
        model = User_cnpj
        fields = ("id", "cnpj", "corporate_name", "segments", "common_user")


class UserCpfReadSerializer(serializers.ModelSerializer):
    common_user = CommonUserReadSerializer()
    civil_status = CivilStatusReadSerializer()
    gender = GenderReadSerializer()

    class Meta:
        model = User_cpf
        fields = (
            "id",
            "cpf",
            "name",
            "last_name",
            "common_user",
            "civil_status",
            "gender",
        )


class LoginCPFReadSerializer(serializers.ModelSerializer):
    common_user = CommonUserLoginSerializer()

    class Meta:
        model = User_cpf
        fields = ("id", "cpf", "name", "last_name", "common_user")


class LoginCNPJReadSerializer(serializers.ModelSerializer):
    common_user = CommonUserLoginSerializer()

    class Meta:
        model = User_cnpj
        fields = ("id", "cnpj", "corporate_name", "segments", "common_user")


class UserCpfInterestReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = User_cpf_interest
        fields = "__all__"


class UserCpfInsertSerializer(serializers.ModelSerializer):

    class Meta:
        model = User_cpf
        fields = "__all__"


class UserCnpjInsertSerializer(serializers.ModelSerializer):

    class Meta:
        model = User_cnpj
        fields = "__all__"


class CommonUserInsertSerializer(serializers.ModelSerializer):

    class Meta:
        model = Common_user
        fields = "__all__"
