from django.urls import path

from base.api.v1.views.answers import (
    UserAnswerListAPIView
)

urlpatterns = [
    path('<int:id>', UserAnswerListAPIView.as_view())
]
