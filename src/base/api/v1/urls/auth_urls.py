from django.urls import path

from base.api.v1.views.auth import (
    AuthDetailAPIView
)

urlpatterns = [
    path('', AuthDetailAPIView.as_view())
]
