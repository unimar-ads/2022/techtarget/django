from django.urls import path

from base.api.v1.views.civil_status import (
    CivilStatusDetailAPIView,
    CivilStatusListAPIView
)

urlpatterns = [
    path('', CivilStatusListAPIView.as_view()),
    path('<int:id>', CivilStatusDetailAPIView.as_view())
]
