from django.urls import path

from base.api.v1.views.user import (
    CommonUserListAPIView,
    CommonUserDetailAPIView,
)

urlpatterns = [
    path("", CommonUserListAPIView.as_view()),
    path("<int:id>", CommonUserDetailAPIView.as_view()),
]
