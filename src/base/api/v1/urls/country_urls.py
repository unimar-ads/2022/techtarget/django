from django.urls import path

from base.api.v1.views.country import (
    CityListAPIView,
    RegionListAPIView,
    StateListAPIView
)

urlpatterns = [
    path('cities', CityListAPIView.as_view()),
    path('states', StateListAPIView.as_view()),
    path('regions', RegionListAPIView.as_view())
]
