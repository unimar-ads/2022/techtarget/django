from django.urls import path

from base.api.v1.views.dashboards import (
    DashDetailAPIView
)

urlpatterns = [
    path('<int:id>', DashDetailAPIView.as_view())
]
