from django.urls import path

from base.api.v1.views.forms import (
    FormsDetailAPIView,
    FormsListView,
    FormsPerUserListAPIView
)

urlpatterns = [
    path('', FormsListView.as_view()),
    path('<int:id>', FormsDetailAPIView.as_view()),
    path('perUser/<int:id>', FormsPerUserListAPIView.as_view())
]
