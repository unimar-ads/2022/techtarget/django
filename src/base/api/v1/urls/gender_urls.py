from django.urls import path

from base.api.v1.views.gender import (
    GenderDetailAPIView,
    GenderListAPIView
)

urlpatterns = [
    path('', GenderListAPIView.as_view()),
    path('<int:id>', GenderDetailAPIView.as_view())
]
