from django.urls import path

from base.api.v1.views.questions import (
    QuestionDetailAPIView,
    QuestionTypesDetailAPIView,
    QuestionTypesListAPIView,
    QuestionsListView
)

urlpatterns = [
    path('<int:id>', QuestionDetailAPIView.as_view()),
    path('types', QuestionTypesListAPIView.as_view()),
    path('types/<int:id>', QuestionTypesDetailAPIView.as_view()),
    path('perForm', QuestionsListView.as_view())
]
