from django.urls import path

from base.api.v1.views.segment import (
    SegmentDetailAPIView,
    SegmentListAPIView
)

urlpatterns = [
    path('', SegmentListAPIView.as_view()),
    path('<int:id>', SegmentDetailAPIView.as_view())
]
