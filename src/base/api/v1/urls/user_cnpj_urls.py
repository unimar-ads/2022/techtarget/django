from django.urls import path

from base.api.v1.views.user import (
    UserCnpjListAPIView
)

urlpatterns = [
    path('filter', UserCnpjListAPIView.as_view())
]
