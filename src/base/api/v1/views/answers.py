from rest_framework.response import Response
from rest_framework import status
from rest_framework import permissions
from rest_framework.views import APIView

from base.api.v1.serializers.forms import (
    UserAnswersReadSerializer
)
from base.models.forms import User_answer
from base.api.v1.views.utils.answers import insert_answer


class UserAnswerListAPIView(APIView):

    serializer_class = UserAnswersReadSerializer
    permission_classes = [permissions.AllowAny]

    def get(self, request, *args, **kwargs):
        queryset = User_answer.objects.all()
        serializer = UserAnswersReadSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, id, *args, **kwargs):

        answers = request.data.get("answers")
        res = insert_answer(id, answers)

        if res is None:
            return Response(
                {"res": "Answer not created"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        return Response(res, status=status.HTTP_201_CREATED)
