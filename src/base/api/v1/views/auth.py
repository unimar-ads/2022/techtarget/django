from rest_framework.response import Response
from rest_framework import status
from rest_framework import permissions
from rest_framework.views import APIView

from base.api.v1.serializers.user import (
    CommonUserLoginSerializer,
    LoginCNPJReadSerializer,
    LoginCPFReadSerializer,
)
from base.models.user import User_cnpj, User_cpf, Common_user


class AuthDetailAPIView(APIView):
    serializer_class = CommonUserLoginSerializer
    permission_classes = [permissions.AllowAny]

    def get(self, request, *args, **kwargs):
        queryset = Common_user.objects.all()
        serializer = CommonUserLoginSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def auth(self, user_email, user_password):
        try:
            user_cnpj = User_cnpj.objects.prefetch_related(
                "common_user"
            ).filter(
                common_user__user_email=user_email,
                common_user__user_password=user_password,
            )
            if len(user_cnpj) == 0:
                return User_cpf.objects.prefetch_related(
                    "common_user"
                ).filter(
                    common_user__user_email=user_email,
                    common_user__user_password=user_password,
                )
            else:
                return user_cnpj

        except User_cnpj.DoesNotExist and User_cpf.DoesNotExist:
            return None

    def post(self, request, *args, **kwargs):

        user_email = request.data.get("user_email")
        user_password = request.data.get("user_password")

        category_instance = self.auth(user_email, user_password)
        if not category_instance:
            return Response(
                {"res": "Object with id does not exists"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        try:
            serializer = LoginCNPJReadSerializer(category_instance[0])
            data = {
                'logged': True,
                'isCompany': True,
                'user': serializer.data
            }
            return Response(data, status=status.HTTP_200_OK)
        except AttributeError:
            serializer = LoginCPFReadSerializer(category_instance[0])
            data = {
                'logged': True,
                'isCompany': False,
                'user': serializer.data
            }
            return Response(data, status=status.HTTP_200_OK)
