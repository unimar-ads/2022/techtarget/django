from rest_framework.response import Response
from rest_framework import status
from rest_framework import permissions
from rest_framework.views import APIView

from base.api.v1.serializers.civil_status import (
    CivilStatusReadSerializer
)

from base.models.civil_status import Civil_stat


class CivilStatusListAPIView(APIView):
    serializer_class = CivilStatusReadSerializer
    permission_classes = [permissions.AllowAny]

    def get(self, request, *args, **kwargs):
        queryset = Civil_stat.objects.all()
        serializer = CivilStatusReadSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):

        data = {
            "status_name": request.data.get("status_name"),
            "status_description": request.data.get("status_description")
        }
        serializer = CivilStatusReadSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CivilStatusDetailAPIView(APIView):
    serializer_class = CivilStatusReadSerializer
    permission_classes = [permissions.AllowAny]

    def get_object(self, obj_id):
        try:
            return Civil_stat.objects.get(id=obj_id)
        except Civil_stat.DoesNotExist:
            return None

    def get(self, request, id, *args, **kwargs):
        category_instance = self.get_object(id)
        if not category_instance:
            return Response(
                {"res": "Object with id does not exists"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        serializer = CivilStatusReadSerializer(category_instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, id, *args, **kwargs):
        category_instance = self.get_object(id)
        if not category_instance:
            return Response(
                {"res": "Object with id does not exists"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        data = {
            "status_name": request.data.get("status_name"),
            "status_description": request.data.get("status_description"),
        }
        serializer = CivilStatusReadSerializer(
            instance=category_instance, data=data, partial=True
        )
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id, *args, **kwargs):
        category_instance = self.get_object(id)
        if not category_instance:
            return Response(
                {"res": "Object with id does not exists"},
                status=status.HTTP_400_BAD_REQUEST
            )
        category_instance.delete()
        return Response(
            {"res": "Object deleted!"},
            status=status.HTTP_200_OK
        )
