from rest_framework import generics, permissions
from base.api.v1.filters.city_filters import CityFilter

from base.api.v1.serializers.country import (
    CityReadSerializer,
    RegionReadSerializer,
    StateReadSerializer
)

from base.models.country import Country_city, Country_region, Country_state


class CityListAPIView(generics.ListAPIView):
    serializer_class = CityReadSerializer
    permission_classes = [permissions.AllowAny]
    queryset = Country_city.objects.filter()
    filterset_class = CityFilter


class StateListAPIView(generics.ListAPIView):
    serializer_class = StateReadSerializer
    permission_classes = [permissions.AllowAny]
    queryset = Country_state.objects.filter()


class RegionListAPIView(generics.ListAPIView):
    serializer_class = RegionReadSerializer
    permission_classes = [permissions.AllowAny]
    queryset = Country_region.objects.filter()
