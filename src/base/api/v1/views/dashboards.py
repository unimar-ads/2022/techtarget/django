from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework import permissions, status
from rest_framework.views import APIView
from base.models.forms import User_answer
from base.api.v1.serializers.forms import (
    QuestionsReadSerializer,
    UserAnswersReadSerializer,
)
from base.api.v1.views.utils.dashboards import (
    per_question
)


class DashDetailAPIView(APIView):

    serializer_class = QuestionsReadSerializer
    permission_classes = [permissions.AllowAny]

    def get(self, request, id, *args, **kwargs):
        queryset = User_answer.objects.select_related().filter(question__id=id)
        serializer = UserAnswersReadSerializer(queryset, many=True)
        serializer = JSONRenderer().render(serializer.data)

        res = per_question(serializer)
        return Response(res, status.HTTP_200_OK)
