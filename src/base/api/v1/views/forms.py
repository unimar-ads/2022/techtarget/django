from rest_framework.response import Response
from rest_framework import status
from rest_framework import permissions
from rest_framework.views import APIView

from base.models.forms import Form, User_answer
from base.api.v1.serializers.forms import (
    FormsReadSerializer,
    QuestionsInsertSerializer,
    UserAnswersReadSerializer,
)


class FormsListView(APIView):
    serializer_class = FormsReadSerializer
    permission_classes = [permissions.AllowAny]

    def get(self, request, *args, **kwargs):
        queryset = Form.objects.all()
        serializer = FormsReadSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):

        questions = request.data.get("questions")
        form_data = {
            "user_cnpj": request.data.get("user_cnpj"),
            "form_title": request.data.get("title"),
            "form_description": request.data.get("description"),
        }
        serializer = FormsReadSerializer(data=form_data)
        if serializer.is_valid():
            serializer.save()
            form_id = serializer.data["id"]

            for question in questions:
                question_data = {
                    "user_form": form_id,
                    "question_type": question["question_type_id"],
                    "question_title": question["question_title"],
                    "question_order": question["question_order"],
                }
                serializer = QuestionsInsertSerializer(data=question_data)
                if serializer.is_valid():
                    serializer.save()

            response_data = {
                "form_id": form_id,
                "mensagem": "Form criado com sucesso",
                "última_questão_inserida": serializer.data
            }
            return Response(response_data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class FormsPerUserListAPIView(APIView):
    serializer_class = FormsReadSerializer
    permission_classes = [permissions.AllowAny]

    def get(self, request, id, *args, **kwargs):
        queryset = Form.objects.all().filter(user_cnpj=id)
        serializer = FormsReadSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class FormsDetailAPIView(APIView):

    serializer_class = FormsReadSerializer
    permission_classes = [permissions.AllowAny]

    def get_object(self, obj_id):
        try:
            return Form.objects.get(id=obj_id)
        except Form.DoesNotExist:
            return None

    def get(self, request, id, *args, **kwargs):
        category_instance = self.get_object(id)
        if not category_instance:
            return Response(
                {"res": "Object with id does not exists"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        serializer = FormsReadSerializer(category_instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, id, *args, **kwargs):
        category_instance = self.get_object(id)
        if not category_instance:
            return Response(
                {"res": "Object with id does not exists"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        data = {
            "form_title": request.data.get("title"),
            "form_description": request.data.get("description"),
        }
        serializer = FormsReadSerializer(
            instance=category_instance, data=data, partial=True
        )
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id, *args, **kwargs):
        category_instance = self.get_object(id)
        if not category_instance:
            return Response(
                {"res": "Object with id does not exists"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        category_instance.delete()
        return Response({"res": "Object deleted!"}, status=status.HTTP_200_OK)


class UserAnswersListView(APIView):
    serializer_class = UserAnswersReadSerializer
    permission_classes = [permissions.AllowAny]

    def get(self, request, *args, **kwargs):
        queryset = User_answer.objects.all()
        serializer = UserAnswersReadSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
