from rest_framework.response import Response
from rest_framework import status
from rest_framework import generics, permissions
from rest_framework.views import APIView
from base.api.v1.filters.question_filters import QuestionFilter

from base.models.forms import Question, Question_type
from base.api.v1.serializers.forms import (
    QuestionTypesReadSerializer,
    QuestionsInsertSerializer,
    QuestionsReadSerializer
)


class QuestionsListView(generics.ListAPIView):
    serializer_class = QuestionsReadSerializer
    permission_classes = [permissions.AllowAny]
    queryset = Question.objects.filter()
    filterset_class = QuestionFilter


class QuestionDetailAPIView(APIView):
    serializer_class = QuestionsReadSerializer
    permission_classes = [permissions.AllowAny]

    def get_object(self, obj_id):
        try:
            return Question.objects.get(id=obj_id)
        except Question.DoesNotExist:
            return None

    def get(self, request, id, *args, **kwargs):
        category_instance = self.get_object(id)
        if not category_instance:
            return Response(
                {"res": "Object with id does not exists"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        serializer = QuestionsReadSerializer(category_instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, id, *args, **kwargs):
        category_instance = self.get_object(id)
        if not category_instance:
            return Response(
                {"res": "Object with id does not exists"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        data = {
            "question_type": request.data.get("question_type_id"),
            "question_title": request.data.get("question_title"),
            "question_order": request.data.get("question_order"),
        }
        serializer = QuestionsInsertSerializer(
            instance=category_instance, data=data, partial=True
        )
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id, *args, **kwargs):
        category_instance = self.get_object(id)
        if not category_instance:
            return Response(
                {"res": "Object with id does not exists"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        category_instance.delete()
        return Response({"res": "Object deleted!"}, status=status.HTTP_200_OK)


class QuestionTypesListAPIView(APIView):
    serializer_class = QuestionTypesReadSerializer
    permission_classes = [permissions.AllowAny]

    def get(self, request, *args, **kwargs):
        queryset = Question_type.objects.all()
        serializer = QuestionTypesReadSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):

        data = {
            "type_title": request.data.get("type_title"),
            "type_description": request.data.get("type_description"),
        }
        serializer = QuestionTypesReadSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class QuestionTypesDetailAPIView(APIView):

    serializer_class = QuestionTypesReadSerializer
    permission_classes = [permissions.AllowAny]

    def get_object(self, obj_id):
        try:
            return Question_type.objects.get(id=obj_id)
        except Question_type.DoesNotExist:
            return None

    def get(self, request, id, *args, **kwargs):
        category_instance = self.get_object(id)
        if not category_instance:
            return Response(
                {"res": "Object with id does not exists"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        serializer = QuestionTypesReadSerializer(category_instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, id, *args, **kwargs):
        category_instance = self.get_object(id)
        if not category_instance:
            return Response(
                {"res": "Object with id does not exists"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        data = {
            "type_title": request.data.get("type_title"),
            "type_description": request.data.get("type_description"),
        }
        serializer = QuestionTypesReadSerializer(
            instance=category_instance, data=data, partial=True
        )
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id, *args, **kwargs):
        category_instance = self.get_object(id)
        if not category_instance:
            return Response(
                {"res": "Object with id does not exists"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        category_instance.delete()
        return Response({"res": "Object deleted!"}, status=status.HTTP_200_OK)
