from rest_framework.response import Response
from rest_framework import status, generics, permissions
from rest_framework.views import APIView
from rest_framework.parsers import MultiPartParser, FormParser, JSONParser
import django_filters
from base.api.v1.filters.user_filters import UserCnpjFilter, UserCpfFilter

from base.api.v1.serializers.user import (
    CommonUserInsertSerializer,
    UserCnpjInsertSerializer,
    UserCnpjReadSerializer,
    UserCpfInsertSerializer,
    UserCpfReadSerializer,
    CommonUserReadSerializer,
)
from base.models.user import User_cnpj, User_cpf, Common_user


class UserCnpjListAPIView(generics.ListAPIView):
    serializer_class = UserCnpjReadSerializer
    permission_classes = [permissions.AllowAny]
    queryset = User_cnpj.objects.filter()
    filterset_class = UserCnpjFilter
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]


class UserCpfListAPIView(generics.ListAPIView):
    serializer_class = UserCpfReadSerializer
    permission_classes = [permissions.AllowAny]
    queryset = User_cpf.objects.filter()
    filterset_class = UserCpfFilter
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]


class CommonUserListAPIView(APIView):
    serializer_class = CommonUserReadSerializer
    permission_classes = [permissions.AllowAny]
    parser_classes = [MultiPartParser, FormParser, JSONParser]

    def get(self, request, *args, **kwargs):
        queryset = Common_user.objects.all()
        serializer = CommonUserReadSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):

        user_type = request.GET["user_type"]
        common_user_data = {
            "user_name": request.data.get("user_name"),
            "user_email": request.data.get("user_email"),
            "user_password": request.data.get("user_password"),
            "user_phone": request.data.get("user_phone"),
            "user_address": request.data.get("user_address"),
            "user_photo": request.data.get("user_photo"),
            "user_city": request.data.get("user_city"),
        }
        serializer = CommonUserInsertSerializer(data=common_user_data)
        if user_type == "cpf" and serializer.is_valid():

            serializer.save()
            user_cpf_data = {
                "common_user": serializer.data["id"],
                "cpf": request.data.get("cpf"),
                "name": request.data.get("name"),
                "last_name": request.data.get("last_name"),
                "gender": request.data.get("gender"),
                "birth_date": request.data.get("birth_date"),
                "profession": request.data.get("profession"),
                "civil_status": request.data.get("civil_status"),
            }
            serializer = UserCpfInsertSerializer(data=user_cpf_data)
            if serializer.is_valid():
                serializer.save()
                return Response(
                    serializer.data, status=status.HTTP_201_CREATED
                )
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST
            )
        elif user_type == "cnpj" and serializer.is_valid():

            serializer.save()
            user_cnpj_data = {
                "common_user": serializer.data["id"],
                "cnpj": request.data.get("cnpj"),
                "corporate_name": request.data.get("corporate_name"),
                "segments": request.data.get("segments"),
            }
            serializer = UserCnpjInsertSerializer(data=user_cnpj_data)
            if serializer.is_valid():
                serializer.save()
                return Response(
                    serializer.data, status=status.HTTP_201_CREATED
                )
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST
            )
        else:
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST
            )


class CommonUserDetailAPIView(APIView):
    serializer_class = CommonUserReadSerializer
    permission_classes = [permissions.AllowAny]

    def get_object(self, obj_id):
        try:
            return Common_user.objects.get(id=obj_id)
        except Common_user.DoesNotExist:
            return None

    def get(self, request, id, *args, **kwargs):
        category_instance = self.get_object(id)
        if not category_instance:
            return Response(
                {"res": "Object with id does not exists"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        serializer = CommonUserReadSerializer(category_instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, id, *args, **kwargs):
        category_instance = self.get_object(id)
        if not category_instance:
            return Response(
                {"res": "Object with id does not exists"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        data = {
            "user_name": request.data.get("user_name"),
            "user_email": request.data.get("user_email"),
            "user_password": request.data.get("user_password"),
            "user_phone": request.data.get("user_phone"),
            "user_address": request.data.get("user_address"),
            "user_photo": request.data.get("user_photo"),
            "user_city": request.data.get("user_city"),
        }
        serializer = CommonUserInsertSerializer(
            instance=category_instance, data=data, partial=True
        )
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id, *args, **kwargs):
        category_instance = self.get_object(id)
        if not category_instance:
            return Response(
                {"res": "Object with id does not exists"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        category_instance.delete()
        return Response({"res": "Object deleted!"}, status=status.HTTP_200_OK)
