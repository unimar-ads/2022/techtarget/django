from base.api.v1.serializers.forms import (
    UserAnswersInsertSerializer,
)


def insert_answer(user_cpf_id, answers):

    list = []
    for i in range(len(answers)):

        key_word = 'answer_field'
        answer = answers[i]

        for word in answer:
            if word.startswith(key_word):
                answer_field = answer[word]

        data = {
            "user_cpf": user_cpf_id,
            "question": answer["question_id"],
            "answer_field": answer_field
        }
        serializer = UserAnswersInsertSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            list.append(serializer.data)
        else:
            list.append(None)
    if None not in list:
        return list
    else:
        return None
