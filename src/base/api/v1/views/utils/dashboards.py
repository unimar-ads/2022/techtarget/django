import io
import pandas as pd
import json
from rest_framework.parsers import JSONParser


def convertjson(lst):
    stream = io.BytesIO(lst)
    data = JSONParser().parse(stream)
    df = pd.DataFrame(data)
    return df


def convertdict(lst):
    df = pd.DataFrame(lst)
    final = df.to_dict()
    return final


def per_question(question):

    list = []
    data = convertjson(question)
    final = {
        "question_title": data["question"][0]["question_title"],
        "question_id": data["question"][0]["id"],
        "results": [],
    }

    try:
        for i in range(len(data["user_cpf"])):

            list.append(
                {
                    "form_id": data["question"][i]["form"]["id"],
                    "question_id": data["question"][i]["id"],
                    "question_type": data["question"][i]["question_type"],
                    "question_title": data["question"][i]["question_title"],
                    "answer": str(data["answer_field"][i]),
                    "user_civil_status": data["user_cpf"][i]["civil_status"][
                        "status_name"
                    ],
                    "user_gender": data["user_cpf"][i]["gender"]["name"],
                    "state": data["user_cpf"][i]["common_user"]["city"][
                        "state"
                    ]["acronym"],
                }
            )
        data = json.dumps(list)
        df = pd.read_json(data)

        answer_values = convertdict(df["answer"].value_counts())
        state_values = convertdict(df["state"].value_counts())
        civil_status_values = convertdict(
            df["user_civil_status"].value_counts()
        )
        gender_values = convertdict(df["user_gender"].value_counts())

        final["results"].append(
            (answer_values, state_values, civil_status_values, gender_values)
        )
        return final
    except:  # noqa
        msg = {"Message": "[INFO] no answers found yet"}
        return msg
