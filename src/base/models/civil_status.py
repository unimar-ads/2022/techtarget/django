from django.db import models


class Civil_stat(models.Model):
    status_name = models.CharField(
        verbose_name="Status name", max_length=255, blank=False
    )
    status_description = models.TextField(
        verbose_name="Status description", max_length=255, blank=True
    )

    def __str__(self):
        return self.status_name
