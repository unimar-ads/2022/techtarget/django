from django.db import models


class Country_region(models.Model):
    region_id = models.IntegerField(verbose_name="Region id", primary_key=True)
    region_name = models.CharField(
        verbose_name="Region name", max_length=255, blank=False
    )

    def __str__(self):
        return self.region_name


class Country_state(models.Model):
    region = models.ForeignKey(
        Country_region, verbose_name="Region", on_delete=models.CASCADE
    )
    state_code = models.IntegerField(
        verbose_name="State code", blank=False, unique=True
    )
    state_name = models.CharField(
        verbose_name="State name", max_length=255, blank=False
    )
    acronym = models.CharField(
        verbose_name="Acronym", max_length=255, blank=False, primary_key=True
    )

    def __str__(self):
        return self.acronym


class Country_city(models.Model):
    state = models.ForeignKey(
        Country_state, verbose_name="State", on_delete=models.CASCADE
    )
    city_code = models.IntegerField(
        verbose_name="City code", blank=False, unique=True
    )
    city_name = models.CharField(
        verbose_name="City name", max_length=255, blank=False
    )

    def __str__(self):
        return self.city_name
