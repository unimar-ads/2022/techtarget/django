from django.db import models


class Gender(models.Model):
    name = models.CharField(verbose_name="Name", max_length=255, blank=False)
    description = models.TextField(
        verbose_name="Description", max_length=255, blank=True
    )

    def __str__(self):
        return self.name
