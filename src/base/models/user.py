import os
from datetime import datetime
from uuid import uuid4
from django.db import models
from django.utils import timezone
from base.models.country import Country_city
from base.models.gender import Gender
from base.models.segment import Segment
from base.models.civil_status import Civil_stat


def path_and_rename_profile(instance, filename):
    upload_to = "profile/"
    ext = filename.split(".")[-1]
    # get filename
    if instance.id:
        filename = "common_user_{0}_{1}.{2}".format(
            instance.id, datetime.now(), ext
        )
    else:
        # set filename as random string
        filename = "{0}_{1}_.{2}".format(uuid4().hex, datetime.now(), ext)
    # return the whole path to the file
    return os.path.join(upload_to, filename)


class Common_user(models.Model):
    user_name = models.CharField(
        verbose_name="User name", max_length=255, blank=False
    )
    creation_date = models.DateTimeField(
        verbose_name="Creation date", default=timezone.now
    )
    user_email = models.EmailField(
        verbose_name="User email", max_length=255, blank=False, unique=True
    )
    user_password = models.CharField(
        verbose_name="User password", max_length=255, blank=False
    )
    user_phone = models.CharField(
        verbose_name="User phone", max_length=255, blank=False
    )
    user_city = models.ForeignKey(
        Country_city, verbose_name="User city", on_delete=models.CASCADE
    )
    user_address = models.CharField(
        verbose_name="User address", max_length=255, blank=False
    )
    user_photo = models.ImageField(
        verbose_name="User photo",
        blank=True,
        null=True,
        upload_to=path_and_rename_profile,
    )
    user_active = models.BooleanField(verbose_name="User active", default=True)

    def __str__(self):
        return self.user_name


class User_cnpj(models.Model):
    common_user = models.ForeignKey(
        Common_user, verbose_name="Common user", on_delete=models.CASCADE
    )
    cnpj = models.CharField(
        verbose_name="CNPJ", max_length=255, blank=False, unique=True
    )
    corporate_name = models.CharField(
        verbose_name="Corporate name", max_length=255, blank=False
    )
    segments = models.ForeignKey(
        Segment,
        verbose_name="Segments",
        on_delete=models.CASCADE,
        related_name="companySegments",
    )

    def __str__(self):
        return self.corporate_name


class User_cpf(models.Model):
    common_user = models.ForeignKey(
        Common_user, verbose_name="Common user", on_delete=models.CASCADE
    )
    cpf = models.CharField(
        verbose_name="CPF", max_length=255, blank=False, unique=True
    )
    name = models.CharField(verbose_name="Name", max_length=255, blank=False)
    last_name = models.CharField(
        verbose_name="Last name", max_length=255, blank=False
    )
    gender = models.ForeignKey(
        Gender, verbose_name="Gender", on_delete=models.CASCADE
    )
    birth_date = models.DateField(
        verbose_name="Birth date", blank=False, default=timezone.now
    )
    profession = models.CharField(
        verbose_name="Profession", max_length=255, blank=True
    )
    civil_status = models.ForeignKey(
        Civil_stat,
        verbose_name="Civil status",
        on_delete=models.CASCADE,
        related_name="civilStatUser",
    )

    def __str__(self):
        return self.name


class User_cpf_interest(models.Model):
    user_cpf = models.ForeignKey(
        User_cpf,
        verbose_name="User cpf",
        on_delete=models.CASCADE,
        related_name="usersInterests",
    )
    segment = models.ForeignKey(
        Segment,
        verbose_name="Segment",
        on_delete=models.CASCADE,
        related_name="segmentsInterests",
    )

    def __unicode__(self):
        return self.user_cpf.name + self.segment.title
