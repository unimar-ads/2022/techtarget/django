from django.contrib import admin
from chat.models import Chat, Room


class ChatAdmin(admin.ModelAdmin):
    list_display = ["owner", "reciever"]
    list_filter = ["owner", "reciever"]
    search_fields = ["owner", "reciever"]


class RoomAdmin(admin.ModelAdmin):
    list_display = ["id"]
    list_filter = ["users"]
    search_fields = ["users"]


admin.site.register(Chat, ChatAdmin)
admin.site.register(Room, RoomAdmin)
