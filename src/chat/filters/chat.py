from django_filters import FilterSet
from chat.models import Chat
from base.api.v1.filters.fields import (
    NumberinFilter
)


class ChatFilter(FilterSet):
    room = NumberinFilter(field_name="room", lookup_expr="in")

    class Meta:
        model = Chat
        fields = ["room"]
