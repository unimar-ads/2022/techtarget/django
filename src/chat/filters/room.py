from django_filters import FilterSet
from chat.models import Room
from base.api.v1.filters.fields import (
    NumberinFilter
)


class RoomFilter(FilterSet):
    users = NumberinFilter(field_name="users", lookup_expr="in")

    class Meta:
        model = Room
        fields = ["users"]
