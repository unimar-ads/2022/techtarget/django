from django.db import models
from base.models.user import Common_user


class Room(models.Model):
    users = models.ManyToManyField(Common_user)

    def __str__(self):
        return "Owners: {0}".format(self.users)


class Chat(models.Model):
    owner = models.ForeignKey(
        Common_user,
        verbose_name="Chat Owner",
        on_delete=models.CASCADE,
        related_name="chat_owner",
    )
    reciever = models.ForeignKey(
        Common_user,
        verbose_name="Chat Reciever",
        on_delete=models.CASCADE,
        related_name="chat_reciever",
    )
    room = models.ForeignKey(
        Room,
        verbose_name="Room",
        on_delete=models.CASCADE,
        related_name="Room",
    )
    content = models.TextField(verbose_name="Content", null=False, blank=False)
    message_date = models.DateTimeField(
        verbose_name="Message Date", auto_now_add=True
    )

    def __str__(self):
        return "Sender: {0} - Time: {1}".format(self.owner, self.message_date)
