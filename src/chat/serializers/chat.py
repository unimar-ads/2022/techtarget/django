from rest_framework import serializers
from chat.models import Chat


class ChatInsertSerializer(serializers.ModelSerializer):

    class Meta:
        model = Chat
        fields = "__all__"


class ChatReadSerializer(ChatInsertSerializer):
    pass
