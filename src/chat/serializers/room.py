from rest_framework import serializers
from base.api.v1.serializers.user import CommonUserReadSerializer
from chat.models import Room


class RoomInsertSerializer(serializers.ModelSerializer):

    class Meta:
        model = Room
        fields = "__all__"


class RoomReadSerializer(RoomInsertSerializer):
    users = CommonUserReadSerializer(many=True)

    class Meta:
        model = Room
        fields = ["id", "users"]
