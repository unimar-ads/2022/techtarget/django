from django.urls import path
from chat.views import testing
from chat.views.chat import ChatFilteredListView
from chat.views.room import (
    RoomDetailAPIView,
    RoomFilteredListView,
    RoomListAPIView,
)


urlpatterns = [
    path("testing", testing.index, name="index"),
    path("testing/<int:room_id>/", testing.room, name="room"),
    path("", ChatFilteredListView.as_view()),
    path("rooms", RoomListAPIView.as_view()),
    path("rooms/filter", RoomFilteredListView.as_view()),
    path("rooms/<int:id>", RoomDetailAPIView.as_view()),
]
