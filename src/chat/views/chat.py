from rest_framework.response import Response
from rest_framework import status, generics, permissions
from rest_framework.views import APIView
from chat.filters.chat import ChatFilter
import django_filters
from chat.serializers.chat import ChatReadSerializer
from chat.models import Chat


class ChatFilteredListView(generics.ListAPIView):
    serializer_class = ChatReadSerializer
    permission_classes = [permissions.AllowAny]
    queryset = Chat.objects.filter()
    filterset_class = ChatFilter
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]


class ChatListAPIView(APIView):

    serializer_class = ChatReadSerializer
    permission_classes = [permissions.AllowAny]

    def get(self, request, *args, **kwargs):
        queryset = Chat.objects.all()
        serializer = ChatReadSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
