from rest_framework.response import Response
from rest_framework import status, generics, permissions
from rest_framework.views import APIView
from chat.filters.room import RoomFilter
import django_filters
from chat.serializers.room import RoomInsertSerializer, RoomReadSerializer
from chat.models import Room


class RoomFilteredListView(generics.ListAPIView):
    serializer_class = RoomReadSerializer
    permission_classes = [permissions.AllowAny]
    queryset = Room.objects.filter()
    filterset_class = RoomFilter
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]


class RoomListAPIView(APIView):

    serializer_class = RoomReadSerializer
    permission_classes = [permissions.AllowAny]

    def get(self, request, *args, **kwargs):
        queryset = Room.objects.all()
        serializer = RoomReadSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):

        room_data = {
            "users": request.data.get("users"),
        }
        serializer = RoomInsertSerializer(data=room_data)
        if serializer.is_valid():
            serializer.save()

            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class RoomDetailAPIView(APIView):
    serializer_class = RoomReadSerializer
    permission_classes = [permissions.AllowAny]

    def get_object(self, obj_id):
        try:
            return Room.objects.get(id=obj_id)
        except Room.DoesNotExist:
            return None

    def get(self, request, id, *args, **kwargs):
        category_instance = self.get_object(id)
        if not category_instance:
            return Response(
                {"res": "Object with id does not exists"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        serializer = RoomReadSerializer(category_instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, id, *args, **kwargs):
        category_instance = self.get_object(id)
        if not category_instance:
            return Response(
                {"res": "Object with id does not exists"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        room_data = {
            "users": request.data.get("users"),
        }
        serializer = RoomInsertSerializer(
            instance=category_instance, data=room_data, partial=True
        )
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id, *args, **kwargs):
        category_instance = self.get_object(id)
        if not category_instance:
            return Response(
                {"res": "Object with id does not exists"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        category_instance.delete()
        return Response({"res": "Object deleted!"}, status=status.HTTP_200_OK)
