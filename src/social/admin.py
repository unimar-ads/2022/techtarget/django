from django.contrib import admin
from social.models import Post, Reaction, PostComment, PostReaction, Follow


class PostAdmin(admin.ModelAdmin):

    list_display = ["post_title"]
    list_filter = ["post_title"]
    search_fields = ["post_title"]


class ReactionAdmin(admin.ModelAdmin):
    list_display = ["slug_name"]
    list_filter = ["slug_name"]
    search_fields = ["slug_name"]


class FollowAdmin(admin.ModelAdmin):
    list_display = ["user_cnpj_followed"]
    list_filter = ["user_cnpj_followed"]
    search_fields = ["user_cnpj_followed"]


class PostCommentAdmin(admin.ModelAdmin):
    list_display = ["user"]
    list_filter = ["user"]
    search_fields = ["user"]


class PostReactionAdmin(admin.ModelAdmin):
    list_display = ["user_reacting"]
    list_filter = ["user_reacting"]
    search_fields = ["user_reacting"]


admin.site.register(Post, PostAdmin)
admin.site.register(Reaction, ReactionAdmin)
admin.site.register(PostComment, PostCommentAdmin)
admin.site.register(PostReaction, PostReactionAdmin)
admin.site.register(Follow, FollowAdmin)
