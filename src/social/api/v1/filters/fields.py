from django_filters import BaseInFilter, CharFilter, NumberFilter


class CharInFilter(BaseInFilter, CharFilter):
    pass


class NumberinFilter(BaseInFilter, NumberFilter):
    pass
