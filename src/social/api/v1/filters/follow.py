from django_filters import FilterSet
from social.models import Follow
from social.api.v1.filters.fields import (
    CharInFilter
)


class FollowFilter(FilterSet):
    user_cpf = CharInFilter(field_name="user_cpf_following", lookup_expr="in")
    user_cnpj = CharInFilter(field_name="user_cnpj_followed", lookup_expr="in")

    class Meta:
        model = Follow
        fields = ["user_cpf", "user_cnpj"]
