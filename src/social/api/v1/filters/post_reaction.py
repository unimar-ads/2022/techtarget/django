from django_filters import FilterSet
from social.models import PostReaction
from social.api.v1.filters.fields import (
    NumberinFilter
)


class PostReactionFilter(FilterSet):
    post_id = NumberinFilter(field_name="post", lookup_expr="in")

    class Meta:
        model = PostReaction
        fields = ["post_id", "user_reacting", "reaction", "reaction_date"]
