from django_filters import FilterSet
from social.models import Post
from social.api.v1.filters.fields import NumberinFilter


class PostFilter(FilterSet):
    owner_id = NumberinFilter(field_name="user_cnpj_owner", lookup_expr="in")

    class Meta:
        model = Post
        fields = [
            "owner_id",
            "post_title",
            "post_description",
            "post_date",
        ]
