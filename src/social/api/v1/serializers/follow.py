from rest_framework import serializers
from social.models import Follow


class FollowReadSerializer(serializers.ModelSerializer):

    class Meta:
        model = Follow
        fields = '__all__'


class FollowInsertSerializer(serializers.ModelSerializer):

    class Meta:
        model = Follow
        fields = '__all__'
