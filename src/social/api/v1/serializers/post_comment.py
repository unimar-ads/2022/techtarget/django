from rest_framework import serializers
from social.models import PostComment


class PostCommentReadSerializer(serializers.ModelSerializer):

    class Meta:
        model = PostComment
        fields = '__all__'


class PostCommentInsertSerializer(serializers.ModelSerializer):

    class Meta:
        model = PostComment
        fields = '__all__'
