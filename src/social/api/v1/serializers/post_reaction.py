from rest_framework import serializers
from base.api.v1.serializers.user import UserCpfReadSerializer
from social.api.v1.serializers.reaction import ReactionReadSerializer
from social.api.v1.serializers.posts import PostReadSerializer
from social.models import PostReaction


class PostReactionReadSerializer(serializers.ModelSerializer):
    user_reacting = UserCpfReadSerializer()
    reaction = ReactionReadSerializer()
    post = PostReadSerializer()

    class Meta:
        model = PostReaction
        fields = ["id", "post", "user_reacting", "reaction", "reaction_date"]


class PostReactionInsertSerializer(serializers.ModelSerializer):
    class Meta:
        model = PostReaction
        fields = "__all__"
