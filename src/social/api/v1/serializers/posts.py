from rest_framework import serializers
from social.models import Post
from base.api.v1.serializers.user import UserCnpjReadSerializer


class PostReadSerializer(serializers.ModelSerializer):
    user_owner = UserCnpjReadSerializer(source="user_cnpj_owner")

    class Meta:
        model = Post
        fields = [
            "id",
            "user_owner",
            "post_title",
            "post_description",
            "post_picture",
            "post_date",
        ]


class PostInsertSerializer(serializers.ModelSerializer):

    class Meta:
        model = Post
        fields = '__all__'
