from rest_framework import serializers
from social.models import Reaction


class ReactionReadSerializer(serializers.ModelSerializer):

    class Meta:
        model = Reaction
        fields = '__all__'


class ReactionInsertSerializer(serializers.ModelSerializer):

    class Meta:
        model = Reaction
        fields = '__all__'
