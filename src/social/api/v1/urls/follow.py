from django.urls import path

from social.api.v1.views.follow import FollowFilterView, FollowListAPIView

urlpatterns = [
    path("", FollowListAPIView.as_view()),
    path("filter", FollowFilterView.as_view())
]
