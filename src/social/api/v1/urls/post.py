from django.urls import path

from social.api.v1.views.posts import PostFilterView, PostListAPIView

urlpatterns = [
    path("", PostListAPIView.as_view()),
    path("filter", PostFilterView.as_view())
]
