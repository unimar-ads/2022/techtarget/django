from django.urls import path

from social.api.v1.views.post_reaction import (
    PostReactionFilterView,
    PostReactionListAPIView
)


urlpatterns = [
    path("", PostReactionListAPIView.as_view()),
    path("filter", PostReactionFilterView.as_view())
]
