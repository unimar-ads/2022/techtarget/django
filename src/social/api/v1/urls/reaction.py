from django.urls import path

from social.api.v1.views.reaction import ReactionListAPIView

urlpatterns = [path("", ReactionListAPIView.as_view())]
