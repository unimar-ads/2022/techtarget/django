from rest_framework.response import Response
from rest_framework import status, generics, permissions
import django_filters
from rest_framework.views import APIView

from social.api.v1.filters.follow import FollowFilter
from social.models import Follow
from social.api.v1.serializers.follow import (
    FollowReadSerializer,
    FollowInsertSerializer,
)


class FollowFilterView(generics.ListAPIView):
    serializer_class = FollowReadSerializer
    permission_classes = [permissions.AllowAny]
    queryset = Follow.objects.filter()
    filterset_class = FollowFilter
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]


class FollowListAPIView(APIView):
    permission_classes = [permissions.AllowAny]

    def get(self, request, *args, **kwargs):
        queryset = Follow.objects.all()
        serializer = FollowReadSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):

        data = {
            "user_cnpj_followed": request.data.get("user_cnpj_followed"),
            "user_cpf_following": request.data.get("user_cpf_following")
        }
        serializer = FollowInsertSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
