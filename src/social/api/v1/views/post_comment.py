from rest_framework.response import Response
from rest_framework import status
from rest_framework import permissions
from rest_framework.views import APIView

from social.models import PostComment
from social.api.v1.serializers.post_comment import (
    PostCommentReadSerializer,
    PostCommentInsertSerializer,
)


class PostCommentListAPIView(APIView):
    permission_classes = [permissions.AllowAny]

    def get(self, request, *args, **kwargs):
        queryset = PostComment.objects.all()
        serializer = PostCommentReadSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):

        data = {
            "post": request.data.get("post"),
            "user": request.data.get("user"),
            "comment": request.data.get("comment")
        }
        serializer = PostCommentInsertSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
