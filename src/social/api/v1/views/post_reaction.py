import django_filters
from rest_framework.response import Response
from rest_framework import status, permissions, generics
from rest_framework.views import APIView
from social.api.v1.filters.post_reaction import PostReactionFilter

from social.models import PostReaction
from social.api.v1.serializers.post_reaction import (
    PostReactionReadSerializer,
    PostReactionInsertSerializer,
)


class PostReactionFilterView(generics.ListAPIView):
    serializer_class = PostReactionReadSerializer
    permission_classes = [permissions.AllowAny]
    queryset = PostReaction.objects.filter()
    filterset_class = PostReactionFilter
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]


class PostReactionListAPIView(APIView):
    permission_classes = [permissions.AllowAny]

    def get(self, request, *args, **kwargs):
        queryset = PostReaction.objects.all()
        serializer = PostReactionReadSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):

        data = {
            "post": request.data.get("post"),
            "user_reacting": request.data.get("user_reacting"),
            "reaction": request.data.get("reaction"),
        }
        serializer = PostReactionInsertSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
