import django_filters
from rest_framework.response import Response
from rest_framework import status, permissions, generics
from rest_framework.views import APIView
from rest_framework.parsers import MultiPartParser, FormParser, JSONParser
from social.api.v1.filters.posts import PostFilter

from social.models import Post
from social.api.v1.serializers.posts import (
    PostReadSerializer,
    PostInsertSerializer,
)


class PostFilterView(generics.ListAPIView):
    serializer_class = PostReadSerializer
    permission_classes = [permissions.AllowAny]
    queryset = Post.objects.filter()
    filterset_class = PostFilter
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]


class PostListAPIView(APIView):
    permission_classes = [permissions.AllowAny]
    parser_classes = [MultiPartParser, FormParser, JSONParser]

    def get(self, request, *args, **kwargs):
        queryset = Post.objects.all()
        serializer = PostReadSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, *args, **Kwargs):
        data = {
            "user_cnpj_owner": request.data.get("user_cnpj_owner"),
            "post_title": request.data.get("post_title"),
            "post_description": request.data.get("post_description"),
            "post_picture": request.data.get("post_picture"),
        }
        serializer = PostInsertSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
