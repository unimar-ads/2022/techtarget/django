from rest_framework.response import Response
from rest_framework import status
from rest_framework import permissions
from rest_framework.views import APIView

from social.models import Reaction
from social.api.v1.serializers.reaction import (
    ReactionReadSerializer,
    ReactionInsertSerializer,
)


class ReactionListAPIView(APIView):
    permission_classes = [permissions.AllowAny]

    def get(self, request, *args, **kwargs):
        queryset = Reaction.objects.all()
        serializer = ReactionReadSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):

        data = {
            "reaction_name": request.data.get("reaction_name"),
            "slug_name": request.data.get("reaction_slug"),
        }
        serializer = ReactionInsertSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
