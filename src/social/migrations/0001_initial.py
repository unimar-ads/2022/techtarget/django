# Generated by Django 4.1 on 2022-09-25 23:27

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ("base", "0006_alter_user_answer_answer_date_creation"),
    ]

    operations = [
        migrations.CreateModel(
            name="Post",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "post_title",
                    models.CharField(max_length=255, verbose_name="Post title"),
                ),
                ("post_description", models.TextField(verbose_name="Post description")),
                (
                    "post_picture",
                    models.ImageField(upload_to="", verbose_name="Post picture"),
                ),
                (
                    "post_date",
                    models.DateTimeField(auto_now_add=True, verbose_name="Post date"),
                ),
                (
                    "user_cnpj_owner",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to="base.user_cnpj",
                        verbose_name="User cnpj owner",
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="Reaction",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "reaction_name",
                    models.CharField(max_length=255, verbose_name="Reaction name"),
                ),
                ("slug_name", models.SlugField(unique=True, verbose_name="Slug name")),
            ],
        ),
        migrations.CreateModel(
            name="PostReaction",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "reaction_date",
                    models.DateTimeField(
                        auto_now_add=True, verbose_name="Reaction date"
                    ),
                ),
                (
                    "post",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to="social.post",
                        verbose_name="Post",
                    ),
                ),
                (
                    "reaction",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to="social.reaction",
                        verbose_name="Reaction",
                    ),
                ),
                (
                    "user_reacting",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to="base.user_cpf",
                        verbose_name="User reacting",
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="PostComment",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("comment", models.TextField(verbose_name="Comment")),
                (
                    "comment_date",
                    models.DateTimeField(
                        auto_now_add=True, verbose_name="Cooment date"
                    ),
                ),
                (
                    "post",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to="social.post",
                        verbose_name="Post",
                    ),
                ),
                (
                    "user",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to="base.user_cpf",
                        verbose_name="User",
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="Follow",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "following_date",
                    models.DateField(auto_now_add=True, verbose_name="Following date"),
                ),
                (
                    "user_cnpj_followed",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to="base.user_cnpj",
                        verbose_name="User cnpj followed",
                    ),
                ),
                (
                    "user_cpf_following",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to="base.user_cpf",
                        verbose_name="User cpf following",
                    ),
                ),
            ],
        ),
    ]
