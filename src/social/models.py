import os
from datetime import datetime
from uuid import uuid4
from django.db import models
from base.models.user import User_cnpj, User_cpf


def image_post_path_and_rename(instance, filename):
    upload_to = "posts/"
    ext = filename.split(".")[-1]
    # get filename
    if instance.id:
        filename = "post_{0}_{1}_.{2}".format(
            instance.id, datetime.now(), ext
        )
    else:
        # set filename as random string
        filename = "{0}_{1}_.{2}".format(uuid4().hex, datetime.now(), ext)
    # return the whole path to the file
    return os.path.join(upload_to, filename)


class Follow(models.Model):
    user_cnpj_followed = models.ForeignKey(
        User_cnpj,
        verbose_name="User cnpj followed",
        null=False,
        blank=False,
        on_delete=models.CASCADE,
    )
    user_cpf_following = models.ForeignKey(
        User_cpf,
        verbose_name="User cpf following",
        null=False,
        blank=False,
        on_delete=models.CASCADE,
    )
    following_date = models.DateField(
        verbose_name="Following date", auto_now_add=True
    )

    def __str__(self):
        return self.user_cpf_following.name


class Reaction(models.Model):
    reaction_name = models.CharField(
        verbose_name="Reaction name", max_length=255, null=False, blank=False
    )
    slug_name = models.SlugField(verbose_name="Slug name", unique=True)

    def __str__(self):
        return self.slug_name

    def __repr__(self):
        return self.reaction_name


class Post(models.Model):
    user_cnpj_owner = models.ForeignKey(
        User_cnpj,
        verbose_name="User cnpj owner",
        null=False,
        blank=False,
        on_delete=models.CASCADE,
    )
    post_title = models.CharField(
        verbose_name="Post title", max_length=255, null=False, blank=False
    )
    post_description = models.TextField(verbose_name="Post description")
    post_picture = models.ImageField(
        verbose_name="Post picture",
        blank=True,
        null=True,
        upload_to=image_post_path_and_rename,
    )
    post_date = models.DateTimeField(
        verbose_name="Post date", auto_now_add=True
    )

    def __str__(self):
        return self.post_title


class PostReaction(models.Model):
    post = models.ForeignKey(
        Post,
        verbose_name="Post",
        null=False,
        blank=False,
        on_delete=models.CASCADE,
    )
    user_reacting = models.ForeignKey(
        User_cpf,
        verbose_name="User reacting",
        null=False,
        blank=False,
        on_delete=models.CASCADE,
    )
    reaction = models.ForeignKey(
        Reaction,
        verbose_name="Reaction",
        null=False,
        blank=False,
        on_delete=models.CASCADE,
    )
    reaction_date = models.DateTimeField(
        verbose_name="Reaction date", auto_now_add=True
    )

    def __str__(self):
        return self.reaction.slug_name


class PostComment(models.Model):
    post = models.ForeignKey(
        Post,
        verbose_name="Post",
        null=False,
        blank=False,
        on_delete=models.CASCADE,
    )
    user = models.ForeignKey(
        User_cpf,
        verbose_name="User",
        null=False,
        blank=False,
        on_delete=models.CASCADE,
    )
    comment = models.TextField(verbose_name="Comment", blank=False, null=False)
    comment_date = models.DateTimeField(
        verbose_name="Cooment date", auto_now_add=True
    )

    def __str__(self):
        return self.post.post_title
