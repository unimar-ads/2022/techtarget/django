from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path("admin/", admin.site.urls),
    path("api/v1/genders/", include("base.api.v1.urls.gender_urls")),
    path("api/v1/civilStatus/", include("base.api.v1.urls.civil_status_urls")),
    path("api/v1/segments/", include("base.api.v1.urls.segment_urls")),
    path(
        "api/v1/users/commonUsers/",
        include("base.api.v1.urls.common_user_urls"),
    ),
    path("api/v1/users/userCpf/", include("base.api.v1.urls.user_cpf_urls")),
    path("api/v1/users/userCnpj/", include("base.api.v1.urls.user_cnpj_urls")),
    path("api/v1/country/", include("base.api.v1.urls.country_urls")),
    path("api/v1/questions/", include("base.api.v1.urls.question_urls")),
    path("api/v1/forms/", include("base.api.v1.urls.form_urls")),
    path("api/v1/auth/", include("base.api.v1.urls.auth_urls")),
    path("api/v1/answers/", include("base.api.v1.urls.answer_urls")),
    path("api/v1/dashboards/", include("base.api.v1.urls.dashboard_urls")),
    path("api/v1/posts/", include("social.api.v1.urls.post")),
    path("api/v1/reactions/", include("social.api.v1.urls.reaction")),
    path("api/v1/postReactions/", include("social.api.v1.urls.post_reaction")),
    path("api/v1/follow/", include("social.api.v1.urls.follow")),
    path("api/v1/chat/", include("chat.urls"))
]


if settings.DEBUG:  # new
    urlpatterns += static(
        settings.MEDIA_URL, document_root=settings.MEDIA_ROOT
    )
